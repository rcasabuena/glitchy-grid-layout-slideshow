import { combineReducers } from 'redux';
import slides from './slides';
import winsize from './winsize';
import mousepos from './mousepos';

export default combineReducers({
  slides,
  winsize,
  mousepos
});