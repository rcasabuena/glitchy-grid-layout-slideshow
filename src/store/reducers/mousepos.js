export const SET_MOUSEPOS = 'SET_MOUSEPOS'

const mousepos = (state = {}, action) => {
  switch (action.type) {
    case SET_MOUSEPOS:
      return action.payload
      break;
  
    default:
      return state
      break;
  }
}

export default mousepos