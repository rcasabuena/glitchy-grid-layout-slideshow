export const SET_ACTIVE = 'SET_ACTIVE'

const slides = (state = {}, action) => {
  switch (action.type) {
    case SET_ACTIVE:
      return state.map(item => {
        return {
          ...item,
          active: item.id == action.payload
        }
      });
      break;
  
    default:
      return state
      break;
  }
}

export default slides