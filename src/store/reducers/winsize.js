export const SET_WINSIZE = 'SET_WINSIZE'

const winsize = (state = {}, action) => {
  switch (action.type) {
    case SET_WINSIZE:
      return action.payload
      break;
  
    default:
      return state
      break;
  }
}

export default winsize