import { getAllSlides } from './slides'
import { getWinsize } from './winsize'
import { getMousepos } from './mousepos'

export default {
  getAllSlides,
  getWinsize,
  getMousepos
}