import { SET_WINSIZE } from '../reducers/winsize'

export const setWinsize = winsize => {
  return {
    type: SET_WINSIZE,
    payload: {...winsize}
  }
} 