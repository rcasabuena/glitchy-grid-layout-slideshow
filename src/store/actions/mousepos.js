import { SET_MOUSEPOS } from '../reducers/mousepos'

export const setMousepos = mousepos => {
  return {
    type: SET_MOUSEPOS,
    payload: mousepos
  }
}