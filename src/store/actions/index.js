import { activateSlide } from './slides'
import { setWinsize } from './winsize'
import { setMousepos } from './mousepos'

export default {
  activateSlide,
  setWinsize,
  setMousepos
}