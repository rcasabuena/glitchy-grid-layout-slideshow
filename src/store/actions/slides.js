import { SET_ACTIVE } from '../reducers/slides'

export const activateSlide = i => {
  return {
    type: SET_ACTIVE,
    payload: i
  }
} 