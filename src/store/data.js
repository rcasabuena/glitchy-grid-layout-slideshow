export const slides = [
  {
    id: '1',
    active: true,
    bgimages: [
      "/img/2.jpg",
      "/img/4.jpg",
      "/img/5.jpg",
      "/img/14.jpg",
      "/img/15.jpg",
      "/img/16.jpg",
      "/img/17.jpg",
      "/img/23.jpg",
    ],
    fonts: {
      font1: {
        text: "Hold fast",
        emphasis: true
      },
      font2: {
        text: "like an anchor",
        emphasis: false
      },
      font3: {
        text: "in the storm",
        emphasis: true
      }
    }
  },
  {
    id: '2',
    active: false,
    bgimages: [
      "/img/1.jpg",
      "/img/9.jpg",
      "/img/3.jpg",
      "/img/10.jpg",
      "/img/11.jpg",
      "/img/12.jpg",
      "/img/13.jpg",
      "/img/22.jpg",
    ],
    fonts: {
      font1: {
        text: "Being free",
        emphasis: true
      },
      font2: {
        text: "is a state",
        emphasis: false
      },
      font3: {
        text: "is a state",
        emphasis: true
      }
    }
  },
  {
    id: '3',
    active: false,
    bgimages: [
      "/img/6.jpg",
      "/img/7.jpg",
      "/img/8.jpg",
      "/img/18.jpg",
      "/img/19.jpg",
      "/img/20.jpg",
      "/img/21.jpg",
      "/img/24.jpg",
    ],
    fonts: {
      font1: {
        text: "There is no final",
        emphasis: true
      },
      font2: {
        text: "one, revolutions",
        emphasis: false
      },
      font3: {
        text: "are infinite",
        emphasis: true
      }
    }
  }
];

export const mousepos = {
  x: 0,
  y: 0
}

export const winsize = {
  width: 0,
  height: 0
}
