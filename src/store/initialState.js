import { slides, winsize, mousepos } from './data';

const initialState = {
  slides,
  winsize,
  mousepos
} 

export default initialState;