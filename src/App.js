import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { preloadImages, preloadFonts, getMousePos, calcWinsize } from './utils';
import Slideshow from './slideshow';
import './App.css'
import actions from './store/actions'
import selectors from './store/selectors'
import Frame from './components/Frame'
import Content from './components/Content'

function App(props) {

  const { winsize, mousepos, setWinsize, setMousepos } = props

  useEffect(() => {
    // Preload  images and fonts
    Promise.all([preloadImages('.content__slide-item', {background: true}), preloadFonts('qhm2ggg')]).then(() => {
      // Remove loader (loading class)
      document.body.classList.remove('loading');
      
      // Initialize stuff
      new Slideshow(document.querySelectorAll('.content__slide')); 
      setWinsize(calcWinsize());
      window.addEventListener('mousemove', ev => setMousepos(getMousePos(ev)));
      window.addEventListener('resize', () => setWinsize(calcWinsize()));
    });
  }, [])

  useEffect(() => {
    setMousepos({x: winsize.width/2, y: winsize.height/2})
  }, [winsize])

  return (
    <>
      <Frame />
      <Content />
    </>
  );
}

const mapStateToProps = state => {
  return {
    winsize: selectors.getWinsize(state),
    mousepos: selectors.getMousepos(state),
  }
}

const mapDispatchToProps = dispatch => {
  return {
    setWinsize: winsize => dispatch(actions.setWinsize(winsize)),
    setMousepos: mousepos => dispatch(actions.setMousepos(mousepos))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App)

