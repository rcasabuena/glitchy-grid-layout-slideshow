import React from 'react'
import { connect } from 'react-redux'
import EnterButton from './EnterButton'
import Slide from './Slide'

function Content(props) {

  const { slides } = props

  return (
    <div className="content">
      {
        slides.map(item => <Slide key={item.id} content={item} />)
      }
      <EnterButton />
    </div>
  )
}

const mapStateToProps = state => {
  return {
    slides: state.slides
  }
}

export default connect(mapStateToProps)(Content)

