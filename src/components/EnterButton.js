import React, { useEffect, useRef, useState } from 'react'
import gsap from 'gsap';
import { lerp, getMousePos, calcWinsize, distance } from '../utils';

function EnterButton(props) {

  const actionBtn = useRef()
  const actionText = useRef()
  // const [translationVals, setTranslationVals] = useState({tx: 0, ty: 0})
  // const [rect, setRect] = useState({width: 0, height: 0}) 
  // const [boundingBox, setBoundingBox] = useState({x: 0, y: 0}) 
  // const [mousepos, setMousePos] = useState({x:0, y:0})

  // const calcRect = () => {
  //   setRect(actionBtn.current.getBoundingClientRect())
  // }

  // useEffect(() => {
  //   calcRect()
  //   window.addEventListener('resize', calcRect);
  //   window.addEventListener('mousemove', ev => setMousePos(getMousePos(ev)));
  //   requestAnimationFrame(() => render());
  // }, [])

  // useEffect(() => {
  //   setBoundingBox({
  //     ...boundingBox,
  //     x: rect.width*0.9, 
  //     y: rect.height*0.9
  //   })
  // }, [rect])

  // const render = () => {
  //   const d = distance(mousepos.x, rect.left + rect.width/2, mousepos.y, (rect.top + rect.height/2));
  //   const x = d < boundingBox.x ? mousepos.x - (rect.left + rect.width/2) : 0;
  //   const y = d < boundingBox.y ? mousepos.y - (rect.top + rect.height/2) : 0;
  //   setTranslationVals({
  //     ...translationVals,
  //     tx: lerp(translationVals.tx, x*.6, 0.17),
  //     ty: lerp(translationVals.ty, y*.6, 0.17)
  //   })
  //   gsap.set(actionBtn.current, {x: translationVals.tx, y: translationVals.ty});
  //   gsap.set(actionText.current, {x: -translationVals.tx*.12, y: -translationVals.ty*.12});
  //   requestAnimationFrame(() => render());
  // }

  return (
    <button ref={actionBtn} className="content__action">
      <span ref={actionText} className="content__action-text">Enter</span>
    </button>
  )
}

export default EnterButton
