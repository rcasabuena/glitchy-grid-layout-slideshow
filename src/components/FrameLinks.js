import React from 'react';

function FrameLinks() {
  return (
    <div className="frame__links">
      <a href="https://bitbucket.org/rcasabuena/glitchy-grid-layout-slideshow" target="_blank">Bitbucket</a>
    </div>
  )
}

export default FrameLinks;
