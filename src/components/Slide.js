import React from 'react'
import classNames from 'classnames'
import SlideItem from './SlideItem'
import SlideFont from './SlideFont'

function Slide(props) {

  const { content } = props

  return (
    <div 
      className={classNames({
        "content__slide": true, 
        "content__slide--current": content.active 
      })}
    >
      {
        content.bgimages.map((item, index) => <SlideItem key={index} bg={item} />)
      }
      <div className="content__slide-text">
        {
          Object.keys(content.fonts).map(key => <SlideFont key={key} font={key} text={content.fonts[key].text} emphasis={content.fonts[key].emphasis} />)
        }
      </div>
    </div>
  )
}

export default Slide
