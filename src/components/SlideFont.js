import React from 'react'

function SlideFont(props) {

  const { font, text, emphasis } = props

  return (
    <span className={`text text--${font}`}>{text}</span>
  )
}

export default SlideFont
