import React from 'react'

function FrameTitle() {
  return (
    <h1 className="frame__title">Glitchy Grid Layout Slideshow</h1>
  )
}

export default FrameTitle
