import React from 'react';

function SlideItem(props) {
  return (
    <div className="content__slide-item" style={{backgroundImage:`url('${props.bg}')`}}></div>
  )
}

export default SlideItem;
