import React, { useRef, useEffect } from 'react'
import { connect } from 'react-redux'
import classNames from 'classnames'
import actions from '../store/actions'
import selectors from '../store/selectors'
import { gsap } from 'gsap';

function FrameNavItem(props) {

  const { slides, activateSlide, slide } = props
  const btn = useRef()

  useEffect(() => {
    navItemEffect(btn.current, slide.active)
  }, [slides])

  function navItemEffect(el, active) {
    gsap
    .timeline()
    .set(el, {transformOrigin: '50% 0%'})
    .to(el, {
        duration: 0.2,
        scaleY: 0
    })
    .set(el, {transformOrigin: '50% 100%'})
    .add(() => el.classList[active ? 'add' : 'remove']('frame__nav-item--current'))
    .to(el, {
        duration: 0.5,
        ease: 'Expo.easeOut',
        scaleY: 1
    }, '+=0.2');
  }

  return (
    <button 
      ref={btn}
      className={classNames({
        "frame__nav-item": true,
        "frame__nav-item--current": slide.active
      })}
      onClick={() => activateSlide(slide.id)}
    >
    </button>
  )
}

const mapStateToProps = state => {
  return {
    slides: selectors.getAllSlides(state)
  }
}

const mapDispatchToProps = dispatch => {
  return {
    activateSlide: i => dispatch(actions.activateSlide(i))
  }
}


export default connect(mapStateToProps, mapDispatchToProps)(FrameNavItem)
