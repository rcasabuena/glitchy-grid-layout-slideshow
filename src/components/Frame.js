import React from 'react'
import FrameTitle from './FrameTitle'
import FrameLinks from './FrameLinks'
import FrameNav from './FrameNav'

function Frame() {
  return (
    <div className="frame">
      <FrameTitle />
      <FrameLinks /> 
      <a className="frame__menu">Menu</a>
      <FrameNav /> 
    </div>
  )
}

export default Frame
