import React from 'react'
import { connect } from 'react-redux'
import selectors from '../store/selectors'
import FrameNavItem from './FrameNavItem'

function FrameNav(props) {
  
  const { slides } = props

  return (
    <nav className="frame__nav">
      {
        slides.map(item => <FrameNavItem key={item.id} slide={item} />)
      }
    </nav>
  )
}

const mapStateToProps = state => {
  return {
    slides: selectors.getAllSlides(state)
  }
}

export default connect(mapStateToProps)(FrameNav)
